waitUntil {!isNull player && player == player};
if(player diarySubjectExists "rules")exitwith{};

//player createDiarySubject ["changelog","Change Log"];
player createDiarySubject ["serverrules","Allgemeine Regeln"];
player createDiarySubject ["fightrules","Kampfregeln"];
player createDiarySubject ["policerules","Polizei Prozeduren/Regeln"];
player createDiarySubject ["gangregeln","Gang Rules"];
player createDiarySubject ["safezones","Sichere Zonen (No Killing)"];
player createDiarySubject ["streets","Straßeverkehrsregeln"];
player createDiarySubject ["rules","Civilistenregeln"];
player createDiarySubject ["punishments","Bussgeldkatalog"];
player createDiarySubject ["illegalitems","Illegale Aktivitaeten"];
player createDiarySubject ["shadowdragons","Altis Sicherheitsdienst"];

player createDiarySubject ["controls","Controls"];
player createDiarySubject ["bugs","Fehler"];
player createDiarySubject ["kompetenzen","Kompetenzen"];
player createDiarySubject ["admins","Der Admin"];






player createDiaryRecord ["serverrules",
	[
	"Allgemeines", 
		"
		Roleplay<br/><br/>

		1: Dies ist ein Rollenspielserver, verhalte dich so<br/>
		2: Ein toter Hamster als Grund für einen Kampf, ist kein Grund<br/>
    	3: Arschloch sein kann Roleplay sein, ist aber Schwer - bedenke dies!<br/>

		"
	]
];
player createDiaryRecord ["serverrules",
	[
	"Spezifisch", 
		"
		Regeln<br/><br/>

		1. Zivilisten haben sich grundlegend an die Anweisungen der Polizei zu halten.<br/>
		2. Weisen Sie den Polizisten ggf. darauf hin, dass Sie Waffen bei sich fuehren.<br/>
		3. Tragen Zivilisten Waffen bei sich, haben sie diese sofort zu senken oder wegzustecken.<br/>
		4. An einem Checkpoint duerfen Polizisten die Zivilisten ohne Angabe von Gruenden durchsuchen.<br/>
		5. Nach der Durchsuchung koennen die Zivilisten in ihr Fahrzeug steigen und wegfahren, solange nichts gegen sie vorliegt.<br/>
		6. Sollte etwas illegales gefunden werden wird die Person festgenommen und abhaengig von dem begangenen Verbrechen zu einer Geld oder Haftstrafe verurteilt.<br/><br/>
		7. Die MEDICS und der ADAC dürfen kein Ziel von Überfallen sein! 
		"
	]
];




player createDiaryRecord ["serverrules",
	[
	"Exploids", 
		"
		Exploits<br/><br/>

		1. Das Entkommen aus dem Knast mit anderen Methoden als die Kaution zu zahlen oder per Helikopter zu fliehen.<br/>
		2. Sich selbst umzubringen um aus schwierigen Situationen, wie zum Beispiel einer Festnahme, dem Knast ect. zu entkommen.<br/>
		3. Das Veruntreuen von grossen Geldsummen. Sollte dir jemand direkt am Anfang des Spiels grosse Summen Geld schicken, melde dies sofort einem Admin, andernfalls kannst du gebannt werden.<br/>
		4. Die Benutzung von gehackten Items. Sollte ein Hacker den Server betreten und Items spawnen, nutze diese nicht, sondern melde es sofort einem Admin.<br/>
		5. Der Missbrauch von Bugs. Sollte man dich beim Ausnutzen von Bugs erwischen, ist dies ein Banngrund.<br/>
		6. Dauerhaftes RDM. Wenn du dauerhaft Random Deathmatch betreibst, ist dies ein Banngrund.<br/><br/>
		"
	]
];


player createDiaryRecord ["serverrules",
	[
	"Banngruende", 
		"
		Betrachtet dies als eure einzige Warnung fuer folgende Faelle.<br/><br/>

		1. Hacking<br/>
		2. Cheating<br/>
		3. Exploiting (unter 'Exploits')<br/>
		4. Drei Kicks fuehren auch zu einem Ban.<br/><br/>
		5. Ein bestehender VAC Ban fuehrt zu sofortigem Ausschluss (Automatisiert)
		6. Das ueerfallen von Spielern waerend man sich Polizist ausgibt.
		7. Permanente Kopfgeldaktionen auf ein und den selben Spieler.
		8. Permanentes Mobbing, Sexuelle Noetigung usw.
		"
	]
];

player createDiaryRecord ["serverrules",
	[
	"Kommunikationsregeln", 
		"
		Die folgenden Taten koennen zu einem Bann fuehren, abhaengig von der Sicht der Admins.<br/><br/>

		1. Der Sidechat darf nur fuer schriftliche Kommunikation benutzt werden.<br/>
		2. Das Spammen von einem Beliebigen Chat Kanal resultiert in einem Bann.<br/>
		3. Teamspeak Kanaele sind mit einem Grund da, Cops muessen sich jederzeit in einem Cop Kanal aufhalten.<br/>
		4. Zivilisten haben in Cop Kanaelen nichts zu suchen! Jeder Zivilist welcher einem Cop Kanal joint wird sofort aus dem Kanal entfernt. Wiederholungstaeter koennen gebannt werden.<br/><br/>
		"
	]
];

player createDiaryRecord ["serverrules",
	[
	"Random Deathmatching (RDM)", 
		"
		Die Folgenden Taten koennen zu einem Bann fuehren, abhaengig von der Sicht der Admins.<br/><br/>

		1. Das Toeten eines Mitspielers ohne Roleplay Grund.<br/>
		2. Cops und Zivilisten duerfen nur in Verbindung mit einem begangenen Verbrechen auf einander Schiessen.<br/>
		3. Wenn du im Kreuzfeuer eines Gefechts erschossen wirst zaehlt dies nicht als RDM.<br/>
		4. Jemanden mit dem Zweck sich selbst zu schuetzen zu toeten zaehlt nicht als RDM (Notwehr).<br/>
		5. Einen Spieler zu erschiessen ohne ihm genuegend Zeit zu geben deinen Anweisungen zu folgen zaehlt als RDM.<br/><br/>

		Jeder Fall wird von einem oder mehreren Admins beurteilt.<br/><br/>
		"
	]
];

player createDiaryRecord["fightrules",
	[
	"Alg. Kampfregeln",
		"
		1: Kämpfe die in Kavala oder Pyrgos starten sind verboten und werden mit KICK oder BAN bestraft!<br/><br/>
		1.1: Grund: Dies sind die Orte an denen neue Spieler spawnen, die sollt ihr nicht direkt erschrecken<br/>
		1.2: Eine Kampfverlagerung nach Kavala wird geduldet, wenn sie RP Technich einwandfrei ist (KEINE HAMSTER)<br/>
		2: Angriffe auf (Polizei)HQs oder Bases zeugen nicht von taktischer Finesse sondern sind Selbstmord!<br/><br/>
		2.2: Grund: Jemanden aus dem Polizei HQ zu befreien und dabei 2 Mitglieder zu verlieren ist kein gutes RP.<br/>
		2.3: Grund: Angriffe auf GangHQs(RS oder DARG) sind immer zum Scheitern verurteilt. Seid Clever und wartet bis sie rauskommen.<br/>
		2.4: Grund: GangHQs sind erklärte Savezones, ausser der Kampf verlagert sich wärend einer Aktion da hin. <br/>
	    3: Warnschüße gelten als BESCHUSS!<br/>
		"
	]
];

player createDiaryRecord["fightrules",
	[
	"Polizei vs. Gang",
		"
		1: Angriffe auf Polizei HQs sind VERBOTEN! <br/><br/>
		1.1: Begründung: Sind Spieler Festgenommen worden und im Polizei HQ festgesetzt ist der Kampf für diese Spieler vorbei!<br/>
		1.2: Befreiungsaktionen beschränken damit sich auf den Gefangenentransport oder das Gefängnis!<br/>
	    2: Die Polizei versucht den Kampf immer zu vermeiden, schießt ihr jedoch Scharf auf die Polizei, MÜSST ihr mit entsprechender Reaktion rechnen.<br/>
	    3: Warnschüße gelten als BESCHUSS!<br/>
	    4: Verhandelt, benehmt euch als würdet ihr am Leben hängen!<br/>
		"
	]
];





player createDiaryRecord ["policerules",
[
	"Zentralbank",
		"
		1.  Die Zentralbank ist ein gesperrtes Gebiet fuer Zivilisten. Zivilisten duerfen die Zentralbank nicht ohne Genehmigung betreten. Tun sie dies doch, werden sie entfernt. Bei wiederholtem Vergehen werden Zivilisten Verhaftet. <br/>
		2.  Im Bereich der Zentralbank herrscht Flugverbotszone!<br/>
		3.  Wird die Zentralbank ausgeraubt, ist es Pflicht jedes verfuegbaren Polizisten zu versuchen, den Raub zu stoppen. Sollten weniger als 5 Polizisten online sein, muessen diese nicht eingreifen.<br/>
		4.  Die eingeteilten Polizisten sollten sich sofort zur Zentralbank begeben, kleine Verbrechen koennen in diesem Fall ignoriert werden.<br/>
		5.  Waffeneinsatz ist gestattet, allerdings sollte jede Moeglichkeit sie lebend gefangen zu nehmen zuerst versucht werden.<br/>
		6.  Die Polizei darf nicht blind in die Bank feuern.<br/>
		7.  Die Polizei sollte Zivilisten in der Bank evakuieren.<br/>
		8.  Jeder Zivilist, der sich in unmittelbarer Umgebung des Bankgelaendes aufhaellt, gilt wahrend eins Bankueberfalls als potentieller Komplize der Bankraeuber.<br/>
		9.  Im Rahmen eines Bankueberfalls gilt die Respawnregel nicht fuer die Polizei.<br/>
		10. Cops duerfen, sollten sie bei einem Bankueberfall sterben, EIN MAL respawnen und erneut am Bankueberfall teilnehmen.<br/>
		"
	]
];

player createDiaryRecord ["policerules",
[
	"Illegale Gebiete", 
	"
		1. Das Rebellengebiet wird nicht als Illegales Gebiet angesehen, sondern als ausserhalb der Zustaendigkeit der Polizei. Diese kann dort mit mind. 3 Polizisten nach eigenem Ermessen eingreifen.<br/>
		2. Cops duerfen illegale (Rot Markiert) Gebiete nicht ohne einen Raid betreten. Patrouillen in diesem Gebiet sind Untersagt!<br/>
		3. Einem Polizist ist es unter keinen Umstaenden erlaubt ein illegales Gebiet zu becampen. (Ausnahme Grenzposten am Rebellengebiet)<br/>
		4. Polizisten duerfen mobile Checkpoints nicht naeher als 1200 Meter an einem illegalen Ort (Drugfields, Drugprocessings) aufbauen.<br/><br/>
		5. Polizisten dürfen Illegale verarbeitungsstätten oder Händler für die dauer von 30 Minuten becampem.<br/>
		6. Polizisten dürfen kein Dauercamp bei den im Punkt 5 genannten Punkten einrichten.<br/>

	"
]
];


player createDiaryRecord["gangregeln",
[
"Gangregeln",
		"
		Gang Politik:<br/><br/>
		Politik zwischen den Organisationen ist sehr wichtig für das Leben auf unserem Insel Staat.<br/>
		<br/><br/>
		Jede Organisation muss einen Head Commander bzw. Diplomaten und Stlv. Diplo haben.<br/>
		Die Identität des Diplomaten muss nach außen bekannt sein, er ist öffentlicher Ansprechpartner für die Bevölkerung / Organisationen und Polizei von Altis.<br/>
		<br/><br/>
		HC = Head Commander einer Organisation<br/>
		Diplo = Diplomat einer Organisation<br/>
		<br/><br/>
		Alle Organisationen haben die Möglichkeit Ressourcen und -geografische Kriege zu führen.<br/>
		<br/><br/>
		Hier findet Ihr alle bekannten Verträge der Oranisationen.<br/>
		<br/><br/>
		VB = Verteidigungsbündnis<br/>
		AUVB; AuVB = Angriffs und Verteidigungsbündnis<br/>
		NAP = Nichtangriffspakt<br/>
		Meta = Bündnis aus mehreren Organisationen (Ähnlichkeit zur NATO)<br/>
		<br/><br/>

		Beispiel:
		<br/>
		[VB] Schließen zwei Gangs ein VB ab, so muss die Gang sich Gegenseitig bei einem Angriff auf einer der beiden Gangs bei der Verteidigung unterstützen.
		<br/>
		[AuVB] Wird einer Gang der Krieg erklärt, so wird automatisch allen Gangs im AuVB-Vertrag den Krieg erklärt. Kill-On-Sight ist in dem Rahmen berechtigt
		<br/>
		[NAP] Einen Nichtangriffspakt abzuschließen bedeutet, das sich beide Gangs nicht mehr angreifen/überfallen sollte es doch zu einem Angriff kommen wird der NAP automatisch hinfällig.
		<br/>
		[Meta] Eine Meta abzuschließen, bedeutet sich mit anderen Organisationen auf lange Zeit zusammen zu tun, um Ziele zu erreichen.	<br/>
	"

]
];

player createDiaryRecord["gangregeln",
[
	"Kampfregeln",
	"
		Sobald 2 Grupperiungen miteinander im Krieg sind greift die Polizei nur ein sobald Zivilisten zu schaden kommen oder das Kriegsgebiet sich auf die Städte ausweitet.<br/>
		Ein Krieg ist kein Grund wahllos andere (nicht beteiligte) Spieler zu töten.<br/>
		Civilisten sollten die Kampfgebiete jedoch meiden.<br/>
		Auch wärend des Krieges muss der Gegenpartei die Möglichkeit gegeben werden eine Waffenruhe oder einen Friedensvertrag zu erwirken.<br/> 
		Ein Krieg ist kein Ausrede für dauerhafte Ballereigeilheit.<br/>
		Die Dauer eine Krieges beträgt MAXIMAL 4 Tage!<br/>
		Zwischen 2 Kriegen müssen mindestens 2 Tage Waffenruhe liegen!<br/>
		Das Startdatum und der Kriegszustand zwischen den Parteien muss im Blog bekannt gegeben werden (http://hero.g3ce.net/blog)<br/>
		<br/>><br/>
		Wärend des Krieges sind die Parteien berechtigt mit allen zur Verfügung stehenden Mitteln zu Kämpfen.<br/>
		Ist deine Fraktion unterlegen, gib einfach auf.<br/>
		Auch im Kampf gelten die RP Regeln.  <br/>
		Zerstörte Fahrzeuge/Hubschrauber/Schiffe werden von den Admins NICHT ersetzt. Ihr Spielt krieg, lebt mit den Konsquenzen. Krieg ist halt teuer!><br/>
		Das Zerstören/Verkaufen von Fahrzeugen/Schiffen und Hubschraubern ist ein erlaubtes Kampfmittel.<br/> WENN SIE DIE PARTEIEN IM KRIEGSZUSTAND BEFINDEN - NUR DANN.
		Die Spieler der Kriegsfraktionen sind nur anzugreifen wenn sie mit CLANTAG erkannt sind. <br/>

		Bsp:<br/> 
		Spieler Horst:<br/>
		Charakter 1: [FOO] imBA-KILLER - ist Ziel<br/>
		Charater 2: ImBA-KILLER - ist kein Ziel - da inkognito/andere Person - Ausnahme ist die (freiwillige) Teilnahme an den Kampfhandlungen <br/>


		Die Kriegserklärung muss von beiden Seiten akzeptiert werden, so wird verhindert das große Gangs die kleinen einfach Plattmachen. <br/>
		Der Krieg ist vorbei sobald eine Partei aufgibt! <br/>
		Die Normalen Regeln des Kampfes (15min usw) gelten weiterhin! <br/>
	"
]
];
/*
player createDiaryRecord["changelog",
[
"Custom Change Log",
"
This section is meant for people doing their own edits to the mission, DO NOT REMOVE THE ABOVE.
"
]
];
*/


player createDiaryRecord["safezones",
[
	"Safe Zones",
	"
		Das absichtliche Sprengen von Fahrzeugen, Raub oder das Toeten anderer Spieler in oder um die folgenden Gebiete wird mit einem Bann bestraft.<br/><br/>

		Jeder Waffenladen Umkreis 100 Meter<br/>
		Alle Polizei Hauptquartiere Umkreis 100 Meter<br/>
		Rebellenstuetzpunkte Umkreis 100 Meter<br/>
		Jedes ATM Umkreis 100 Meter<br/><br/>
		Jede Rebellenbasis<br/><br/>

		Kavala und PYRGOS!<br/>
		<br/>
		Befindet man sich in einer Verfolgungsjagd, gelten Safezones nicht als Safezones! (Gilt fuer Cops wie fuer Zivilisten)<br/>
		D. h. das Retten in eine Safezone ist nicht erlaubt!<br/><br/>
	"
]
];



player createDiaryRecord ["streets",
[
"Geschwindigkeitsbegrenzung", 
	"
	Folgende Geschwindigkeitsbegrenzungen gelten in ganz Altis:<br/><br/>
	Marktplatz: 30km/h<br/>
	Innerorts: 50km/h<br/>
	Ausserorts: 100km/h<br/>
	Highway: 130km/h<br/><br/>
	"
]
];

player createDiaryRecord ["rules",
[
"Waffen", 
"
Einem Polizisten ist es niemals erlaubt Zivilisten mit Waffen zu versorgen. Dies wird mit einer Suspendierung geahndet.<br/><br/>

Legale Waffen fuer Zivilisten sind:<br/>
1. P07<br/>
2. Rook<br/>
3. ACP-C2<br/>
4. SDAR 9mm<br/>
6. PDW2000<br/><br/>

Jede andere Waffe ist illegal.<br/><br/>

1. Zivilisten ist es nicht erlaubt innerhalb einer Stadt eine Waffe offen zu tragen.<br/>
2. Zivilisten koennen ausserhalb von Staedten legale Waffen tragen, doch sollten sie auf Nachfrage bereit sein, dem Polizisten ihre Lizensen zu zeigen.<br/><br/>
"
]
];

player createDiaryRecord ["rules",
[
"Nicht toedliche Gewalt",
"
Zurzeit ist der Taser die einzige Form nicht toedlicher Gewalt.<br/><br/>

1. Der Taser soll dazu genutzt werden Zivilisten welche sich den Anweisungen wiedersetzen ruhig zu stellen oder zu verhaften.<br/>
2. Das unangebrachte Feuern des Tasers fuehrt zu einer Suspendierung.<br/>
3. Benutze den Taser nur um das Gesetz zu wahren, nicht um anderen deinen Willen aufzuzwingen.<br/><br/>
"
]
];

player createDiaryRecord ["punishments",
[
"Bussgeldkatalog",
"
<b>Bussgeldkatalog</b><br/>
1. Bussgelder muessen den Umstaenden angepasst werden <br/>und es duerfen beispielsweise keine 100K fuer eine Geschwindigkeitsuebertretung ausgestellt werden.<br/>
2. Verweigerung der Zahlung eines Bussgeldes, kann zur <br/>Gefaengnisstrafe fuehren.<br/>
3. Bei Unsicherheit ueber den Preis eines Tickets, wird <br/>ein ranghoeherer Polizist gefragt bzw hinzu gezogen.<br/><br/>

<b>Paragraph 1.Allgemeine Verkehrsregeln</b><br/>
Abs1. Geschwindigkeitsuebertretung = 500$ - 5.000$ <br/>(nach eigenem Ermessen)<br/>
Abs2. Fahren ohne Licht = 1.000$<br/>
Abs3. Fahren ohne gueltigen Fuehrerschein = PKW = 500$ <br/>LKW = 5.000$ Luftfahrzeuge =10000 Wasserfahrzeuge = 10000<br/>
Abs4. Fahren auf der Falschen Strassenseite = 2.000$<br/>
Abs5. Fahren abseits der Wege (Ausgenommen Ressourcenfelder) = 3.000$<br/>
Abs6. Behindern des Verkehrs = 3.000$<br/>
Abs7. Mutwilliges Zerstoeren von Fremden Eigentum <br/>mit Fahrzeugen = Entzug des Fahrzeuges + 5.000$<br/><br/> 
Paragraph 2.Waffenbesitz<br/>
Abs1. Waffenbesitz ohne Gueltigen Waffenschein = 10.000$ + Entzug der Waffe<br/>
Abs2. Illegaler Waffenbesitz = 15.000$ + Entzug der Waffe<br/>
Abs3. Illegaler Sprengstoffbesitz = 30.000$ + Gefaengnisstrafe<br/>
Abs4. oeffentlich in Kavalar mit Schusswaffe rum laufen = 20.000$ Strafe und einziehen der Waffe. (Ausnahmen Polizei und Security)<br/><br/>

<b>Paragraph 3.Drogenbesitz</b><br/>
Abs1. Drogenfund = 25.000$ + sofortige Gefaengnisstrafe ab Kleintransporter (Truck)<br/>
Abs2. Drogenkonsum = 1.000$<br/><br/>

<b>Paragraph 4. Mord und Raub</b><br/>
Abs1. Raub = 15.000$ bis zu Gefaengnisstrafe<br/>
Abs2. Koerperverletzung = 5000$<br/>
Abs3. Unfall mit Todesfolge = 10.000$<br/>
Abs4. Versuchter Mord = 20.000$ + Gefaengnisstrafe<br/>
Abs5. Mord = 50.000$ + Gefaengnisstrafe<br/>
Abs6. Mord an einen Polizeibeamten 100000$ Strafe und Gefaengnisstrafe<br/>
Abs7. Versuchter Mord an einen Polizeibeamten 50000$ Strafe und Gefaengnisstrafe<br/><br/>

<b>Paragraph 5. sonstige Vergehen</b><br/>
Abs1. versuchter Auto diebstahl 3.500$<br/>
Abs2. Fahren ohne gueltige zulassung auf das jeweilige <br/>fahrzeuges = Fahrzeug wird beschlagnahmt und 3500$ Strafe<br/>
Abs3. Illegales betreten von sperrgebieten = 1.000$ + Platzverweiss<br/>
Abs4. Erpressen von Geldern oder fahrzeugen = 15.000$ (nach Absprache mit dem Rang hoechsten Beamten)<br/>
Abs5. Beleidigungen gegenueber Beamten oder Zivilisten = 500$ - 5.000$ (nach eigenem Ermessen)<br/>
Abs6. Geiselnahmen = 50.000$<br/>
Abs7. Bankueberfall = 150.000$ je Rebel der daran beteiligt ist<br/>
Abs8. Illegaler Fahrzeugbesitz 100000$ Strafe. Fahrzeug wird eingezogen.<br/><br/>
Andere hier nicht aufgefuehrte Straftaten werden bitte nach <br/>eigenem Ermessen erhoben. Ggf einen Ranghoereren Beamten hinzu ziehen.<br/>
"
]
];




player createDiaryRecord ["rules",
[
"Gang Regeln",
"
1. Es ist nicht illegal, in einer Gang zu sein.<br/>
2. Es ist nicht illegal, sich in einem Gang Gebiet aufzuhalten, nur, wenn man an illegalen Aktivitaeten beteiligt ist.<br/><br/>"
]
];

player createDiaryRecord ["rules",
[
"Illegale Fahrzeuge",
"
Es ist fuer Zivilisten illegal diese Fahrzeuge zu besitzen.<br/><br/>

1. Ifrit<br/>
2. Speedboat<br/>
3. Hunter<br/>
4. bewaffneter Offroad<br/>
5. Police Offroad<br/>
6. Strider<br/>
"
]
];

// SD Regeln
player createDiaryRecord ["shadowdragons",
[
"Altis Sicherheitsdienst",
"
1. Der Sicherheitsdienst ist eine Zivile Einrichtung<br/>
2. Der Sicherheitsdienst darf - auf und nur auf Anforderung - die Polizeikraefte auch mit Gewalt ueberstuetzen<br/>
3. Das Offizielle Dienstfahrzeug ist der Ifrit Schwarz.<br/>
3. Bewaffnete Fahrzeuge duerfen nur in Absprache mit der Polizei benutzt werden und dann auch nur ausserhalb der Staedte.<br/>
4. Der Sicherheitsdienst darf nicht mehr als 35% Gewinnbeteiligung fordern.<br/>
5. Illegale Aktivitaeten mit den Dienstfahrzeugen sind zu unterlassen<br/>
6. Der Sicherheitsdienst darf nur selbststaendig Gewalt anwenden wenn eine Bedrohung des eigenen Lebens, des Lebens des Klienten oder unbeteiligter Personen in Gefahr ist.<br/>
7. Aktivitaeten mit Waffeneinsatz sind sofort bzw. spaetestens nach beendigung der Polizei zu melden<br/>
8. Kopfgeldjagt im Auftrag der Polizei ist erlaubt - Oberstes Ziel ist die moeglichst gewaltlose Festname des Gesuchten<br/>
9. Die Altis Sicherheitsdienstbasis ist immer zu beschuetzen - notfalls mit Waffengewalt<br/>
10. Waffeneinsatz zum Stoppen von Fahrzeugen muss sich auf die Reifen beschraenken. Ziel ist es das Fahrzeug zu Stoppen<br/>
11. Er hat die befugnuis Fahrzeuge im Einsatz zu durchsuchen<br/>
12. Sollte der Klient ein illegaler Kurrier sein, darf der Sicherheitsdienst diesen gegen alle Gefahren beschuetzen an denen die Polizei nicht beteiligt ist!<br/>
13. Der Sicherheitsdienst ist blind gegenueber dem Klienten, solange die Polizei nicht beteiligt ist.<br/>
14. Das Offizielle Luftfahrzeug ist der Orca in Schwarz und der Mohawk in Praesidentenfarbe.<br/>
"
]
];


// Controls Section

player createDiaryRecord ["controls",
[
"",
"
Y: Oeffnet Spieler Menue<br/>
U: Fahrzeug Auf- und zu sperren<br/>
F: Polizei Sirene (Cop)<br/>
T: Fahrzeug Inventar<br/>
O: Schranken öffnen<br/>
Left Shift + R: Festnehmen (Cop)<br/>
Left Shift + G: Ausknocken (Civ)<br/>
Left Windows: Haupt Interaktionstaste - Geld aufheben etc | Repairmenue etc. (Use Action 10 - kann umgebindet werden)<br/>
Left Shift + L: Polizei Lichter (Cop)<br/>
"
]
];

player createDiaryRecord ["Bugs",
[
"Fehler im Spiel", 
"
1: Solltest du Opfer eines Bugs werden ist das zwar doof, aber leider passiert das.<br/>
2: Es gibt keinen Ersatz dafuer - ausser dieser Bug ist bekannt und durch einen Admin verursacht.<br/>
3: Es besteht kein Anspruch auf Erstattung - das ist Adminermessen!<br/>

"
]
];


player createDiaryRecord ["admins",
[
"Der Admin", 
"
Hat immer recht!<br/>
"
]
];
player createDiaryRecord ["kompetenzen",
[
"Ansprechpartner", 
"
Admin: <br/>
1: Richard Castle/[SD] Demon/ ONeil <br/>

Rcon Admins: <br/>
Einige ;) <br/><br/>

Polizeiprasidenten: <br/>
1: H.Heinl <br/>
2: Fumiko <br/><br/>
3: Jubii <br/><br/>

"
]
];
